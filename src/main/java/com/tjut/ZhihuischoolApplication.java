package com.tjut;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZhihuischoolApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZhihuischoolApplication.class, args);
	}

}
