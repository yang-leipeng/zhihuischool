package com.tjut.mapper;

import com.tjut.pojo.Clazz;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tjut.pojo.Clazz;
import org.springframework.stereotype.Repository;

@Repository
public interface ClazzMapper extends BaseMapper<Clazz> {
}
